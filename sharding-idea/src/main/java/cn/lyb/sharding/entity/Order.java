package cn.lyb.sharding.entity;

import javax.persistence.*;

@Entity
@Table(name = "t_order")
public class Order {

    @Id
    @Column(name = "order_id")
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderId;

    @Column(name= "user_id")
    private Long userId;


    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }






}
